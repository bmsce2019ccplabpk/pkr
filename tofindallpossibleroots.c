#include<stdio.h>
#include<math.h>
int main()
{
    float r1,r2,a,b,c,dis;
    printf("Enter coefficients of quadratic equation in the form of a*x*x+b*x+c\n");
    scanf("%f%f%f",&a,&b,&c);
    dis=b*b-4*a*c;
    if(dis==0)
    {
      r1=-b/2*a;
      printf("Roots are real and equal r1=%f ",r1);
    }
    else if(dis>0)
    {
       r1=(-b+sqrt(dis))/2*a;
       r2=(-b-sqrt(dis))/2*a;
       printf("Roots are distinct and real r1=%f and r2=%f",r1,r2);
    }
    else
    {
      printf("roots are imaginary and complex\n");
    }
return 0;
}